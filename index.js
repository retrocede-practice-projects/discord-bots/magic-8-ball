require('dotenv').config();
const answers = require('./answers.js');
const Discord = require('discord.js');
const client = new Discord.Client();

client.once('ready', () => {
    client.user.setActivity("the Future", { type: "WATCHING" });
});

client.on('message', msg => {
    if (msg.content.match(/-\s*shake?y/)) {
        msg.channel.startTyping();
        const embed = new Discord.MessageEmbed()
            .setColor("#6A0DAD")
            .setDescription(answers[Math.floor(Math.random() * answers.length)]);

        msg.channel
            .send(embed)
            .finally(() => msg.channel.stopTyping());
    }
});

client.login(process.env.BOT_TOKEN);